import socket,struct,sys
import pandas as pd
from impala.dbapi import connect


#读取数据文件
# orgin_data=pd.read_csv("./1/capture20110810.binetflow")
# 从hive数据库中读取所需要的数据
def Get_Data():
    conn=connect(host="192.168.142.12",port=10000,user="root",password="123456",database="ctu13",auth_mechanism="PLAIN")
    cur=conn.cursor()
    sql="select * from ctu_13_data005"
    cur.execute(sql)
    orgin_data=[]
    protoList = []#proto字段列表
    stateList=[]#state字段列表
    dirList=[]#dir字段列表
    num=0
    for i in cur.fetchall():
        num+=1
        if num%1000==0:
            print("转存数据：",num)
        if i[2] not in protoList:
            protoList.append(i[2])
        if i[8] not in stateList:
            stateList.append(i[8])
        if i[5] not in dirList:
            dirList.append(i[5])
        orgin_data.append([i[0],i[1],i[2],i[3],i[4],i[5],i[6],i[7],i[8],i[9],i[10],i[11],i[12],i[13],i[14]])
    cur.close()
    conn.close()
    return orgin_data,protoList,stateList,dirList

def make_data(data,protoList,stateList,dirList):
    #将数据转化为DataFrame格式
    orgin_data=pd.DataFrame(data=data[1:],columns=data[0])
    print(orgin_data.head())


    # 将proto字段的值转化为整型数值
    # protoDict = {'tcp': 1, 'udp': 2, 'rtp': 3, 'pim': 4, 'icmp': 5, 'arp': 6, 'ipx/spx': 7, 'rtcp': 8, 'igmp': 9,
    #              'ipv6-icmp': 10, 'ipv6': 11, 'udt': 12, 'esp': 13, 'unas': 14, 'rarp': 15}
    protoDict={}
    for i in range(len(protoList)):
        protoDict[protoList[i]]=i

    # 同上
    stateDict = {}
    # stateList = ['S_RA', 'SR_A', 'SR_SA', 'SRPA_SPA', 'RA_', 'SRPA_FSPA', 'FSRPA_FSPA', 'FSA_FSA', 'S_R', 'SRA_SA', 'CON',
    #              'FA_A', 'FA_FA', 'PA_PA', 'FPA_FRPA', 'FPA_RPA', 'FSPA_FSPA', 'PA_A', 'FA_RA', 'PA_FRA', 'FRPA_FPA',
    #              'FPA_FA', 'A_', 'INT', 'FA_', 'S_SA', 'RPA_RPA', 'FPA_FPA', 'PA_RPA', 'FSPA_FSRPA', 'S_', 'SA_R', 'FPA_',
    #              'RPA_PA', 'URP', 'SRPA_FSRPA', 'FRPA_PA', 'URN', 'PA_', 'FA_FPA', 'SPA_FSPA', 'PA_FRPA', 'PA_PAC',
    #              'PA_FPA', 'FRPA_RA', 'TXD', 'RA_FA', 'FRA_', 'ECO', 'FRA_FPA', 'URH', 'SA_', 'SRPA_SRA', 'RPA_FPA',
    #              'FSRPA_SPA', 'PA_R', 'RA_A', 'RED', 'SPA_SRPA', 'FA_R', 'RA_FPA', 'A_PA', 'ECR', 'FSPA_FSRA', 'SPA_SPA',
    #              'FSRPA_FSRPA', 'SRA_FSA', 'FPA_PA', 'FRPA_RPA', 'FPA_RA', 'FPA_FRA', 'FSPA_FSA', 'FRA_FA', 'URFIL',
    #              '_FSPA', 'R_', 'FA_FRPA', 'SPA_FSRPA', 'FA_FRA', 'RPA_FRPA', 'RPA_R', 'A_A', 'A_RA', 'SRPA_SA', 'SRA_RA',
    #              'FSPA_SPA', 'RPAC_PA', 'SPA_SRA', 'FSPAC_FSPA', 'PA_RA', 'FRPA_FA', 'A_R', 'SRPA_SRPA', 'RSP', '_FSA',
    #              'URHPRO', 'FSRA_FSA', 'FPA_R', 'SR_', 'FSRPA_SA', '_SRA', 'FSPA_SRPA', 'SA_FSA', 'FRPA_', 'SPA_SA',
    #              'FPA_A', 'FSRPA_FSA', '_SPA', 'FS_SA', 'SR_RA', 'FSRA_SA', 'SA_SRA', 'FSA_SRA', 'FRPA_FRPA', 'SRPA_FSA',
    #              'SPAC_SPA', 'URF', 'SA_RA', 'FRA_R', 'FSPA_SRA', '_SRPA', 'SPA_FSRA', 'S_SRA', 'FSA_FSRA', 'NNS',
    #              'FSRPA_SRPA', 'S_FRA', 'SR_SRA', 'NRS', 'FSA_FSPA', 'SRA_', 'FAU_R', 'FS_', 'FSRPAE_FSPA', 'FSRPAEC_FSPA',
    #              'FSRA_FSPA', 'FSPA_SA', 'FSAU_SA', 'FSR_SA', 'DNP', 'FSRPAC_FSPA', 'SRA_SRA', 'FSRPA_FSRA', 'FSPA_FSPAC',
    #              'FSA_SA', '_SA', 'FSRPA_SRA', 'FSAU_FSA', 'URO', 'RA_S', 'SA_RPA', 'SEC_', 'RPA_', 'SA_SPA', 'SRA_FSPA',
    #              'SPAC_SRPA', 'SPAC_FSPA', 'SA_FSPA', 'A_RPE', 'SRC', 'S_RPA', 'SRPAC_FSPA', 'SRPA_FSRA', 'FSPAC_FSRPA',
    #              'FSPA_FSRPAC', '', 'REQ', 'FSPAC_FSA', 'FSPAC_FSRA', 'DCE', 'FPA_FSPA', 'SRPAC_SPA', '_PA', 'SA_SA',
    #              'SR_FSA', 'SPA_SRPAC', 'FSPAC_SRPA', 'FSRA_SRA', 'SEC_RA', '_R', 'FSRPAC_FSRPA', 'SA_SRPA', 'PAC_RPA',
    #              'FSAU_SRA', 'S_FA', 'FSA_', 'RPA_A', 'RA_PA', 'URNPRO', 'SRA_SPA', 'FSRPAE_FSA', '_FPA', 'SPAC_FSRPA',
    #              'SRPA_SPAC', 'FRPA_FSPA', 'FSPA_FPA', 'SPA_', 'SA_FSRA', 'RPA_FSPA', 'FSAU_FSRA', 'FSPA_FA', 'FSRA_SPA',
    #              'SPA_FSA', 'FSPAC_SRA', 'FSA_FPA', 'MRQ', 'SA_FSRPA', 'FSRPAC_SPA', 'FRA_PA', '_FRPA', 'SREC_SA', 'A_RPA',
    #              'PAC_PA', 'SA_FR', 'FSRA_FSRPA', 'FSPAC_SPA', 'UNK', 'FSA_FSRPA', '_FA', '_FSRPA', 'S_A', 'FPAC_FPA',
    #              'FPA_FSRPA', 'FSRA_FSRA', 'SRPA_FSPAC', 'SA_A', 'PA_SRA', 'FSRAU_FSA', '_RA', 'FSPAEC_FSPAE', 'FRPA_R']

    for i in range(len(stateList)):
        stateDict[stateList[i]] = i

    # 创建一个字段，用于保存Dir字段信息，并方便进行转换
    # dirDict = {'   ->': 1, '   ?>': 2, '  <->': 3, '  <?>': 4, '  who': 5, '  <-': 6, '  <?': 7}
    dirDict={}
    for i in range(len(dirList)):
        dirDict[dirList[i]]=i

    #选择可能有价值数据
    orgin_data_first=orgin_data[["Dur","Proto","Sport","Dir","Dport","SrcAddr","DstAddr","TotPkts","TotBytes","Label","State"]]

    #创建一个函数make_Addr，用于处理数据中的源目的ip地址
    def make_Addr(x):
    #     print(x)
    #     SrcAddr=socket.inet_aton(x)
    #     SrcAddr=struct.unpack("!L",SrcAddr)[0]#!L好像是表示按照原字符长度进行长整型转换，得到的是一个元组，因此获取第0个数据为真实数据。
        try:
            Addr=struct.unpack("!L",socket.inet_aton(x))[0]
            # print(Addr)
            return Addr
        except:
            return 0

    #将ip地址信息转换为整形数据
    orgin_data_first["SrcAddr"]=orgin_data_first.apply(lambda x: make_Addr(x.SrcAddr),axis=1)
    print("SrcAddr处理完成，开始处理DstAddr")

    #将ip地址信息转化为整形数据
    orgin_data_first["DstAddr"]=orgin_data_first.apply(lambda x: make_Addr(x.DstAddr),axis=1)
    print("DstAddr处理完成，开始处理空字段信息")

    #对数据标签进行处理，非僵尸网络标记为0，僵尸网络标记为1
    def make_Label(x):
        if "Background" in x:
            return 0
        elif "Normal" in x:
            return 0
        elif "Botnet" in x:
            return 1
        else:
            return -1

    #开始处理标签字段
    orgin_data_first["Label"]=orgin_data_first.apply(lambda x: make_Label(x.Label),axis=1)

    # 将Proto和State字段中的值替换为字典中对应的整数值
    def make_Proto(x):
        return protoDict[x]
    def make_Spoto(x):
        return stateDict[x]

    # 将Proto和State字段中的值替换为字典中对应的整数值
    orgin_data_first["Proto"]=orgin_data_first.apply(lambda x: make_Proto(x.Proto),axis=1)
    orgin_data_first["State"]=orgin_data_first.apply(lambda x: make_Spoto(x.State),axis=1)

    # 将Dir字段中的值替换为对应字典中的整数值
    def make_Dir(x):
        return dirDict[x]
    orgin_data_first["Dir"]=orgin_data_first.apply(lambda x: make_Dir(x.Dir),axis=1)

    # 处理字段为空信息数据
    # try_num=0
    # for i in range(len(orgin_data_first)):
    #     try:
    #         #
    #         # 对数据标签进行处理，非僵尸网络标记为0，僵尸网络标记为1
    #         if "Background" in orgin_data_first["Label"][i]:
    #             orgin_data_first["Label"][i] = 0
    #         elif "Normal" in orgin_data_first["Label"][i]:
    #             orgin_data_first["Label"][i] = 0
    #         elif "Botnet" in orgin_data_first["Label"][i]:
    #             orgin_data_first["Label"][i] = 1
    #
    #         # 将Proto和State字段中的值替换为字典中对应的整数值
    #         orgin_data_first["Proto"][i] = protoDict[orgin_data_first["Proto"][i]]
    #         orgin_data_first["State"][i] = stateDict[orgin_data_first["State"][i]]
    #
    #         # 将Dir字段中的值替换为对应字典中的整数值
    #         orgin_data_first["Dir"][i] = dirDict[orgin_data_first["Dir"][i]]
    #
    #         if i % 10000 == 0:
    #             print(i)
    #     except:
    #         try_num+=1
    #         print("出错数：",try_num)

    #将字符串数据转换为int数据格式数据
    def str2int(x):
        try:
            return int(x)
        except:
            print("错误数据：", x)

    #调用str2int函数将“TotBytes字段和TotPkts字段的值”
    orgin_data_first["TotBytes"]=orgin_data_first.apply(lambda x:str2int(x.TotBytes),axis=1)
    orgin_data_first["TotPkts"]=orgin_data_first.apply(lambda x:str2int(x.TotPkts),axis=1)
    return orgin_data_first

def save_data(orgin_data_first):
    # 将训练集和测试集进行保存
    orgin_data_first.to_csv("./train_data/orgin_data_first005.csv")


if __name__ == '__main__':
    data,protoList,stateList,dirList = Get_Data()
    orgin_data_first=make_data(data,protoList,stateList,dirList)
    save_data(orgin_data_first)