import socket,struct,sys
import numpy as np
import pickle
import pandas as pd
from sklearn.linear_model import *
from sklearn.tree import *
from sklearn.naive_bayes import *
from sklearn.neighbors import *
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier,GradientBoostingClassifier,AdaBoostClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis,QuadraticDiscriminantAnalysis
from sklearn.metrics import mean_squared_error,accuracy_score,precision_score,recall_score,f1_score
import joblib



#数据加载函数
def load_data(filename):
    orgin_data=pd.read_csv(filename)
    return orgin_data

#字段选取函数
def Selected_fields(orgin_data):
    orgin_data_first=orgin_data[["Dur","Proto","Sport","Dir","Dport","SrcAddr","DstAddr","TotPkts","TotBytes","Label","State"]]
    return orgin_data_first

#划分数据集和测试集，并划分测试集和目标值
def x_y_data(orgin_data_first):
    x_data = orgin_data_first[["Dur", "Proto", "Dir", "TotPkts", "TotBytes", "State"]]
    y_data = orgin_data_first["Label"]
    x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.3)
    return x_train, x_test, y_train, y_test

#标准化函数
def stand_data(x_train,x_test):
    std=StandardScaler()
    x_train=std.fit_transform(x_train)
    x_test=std.fit_transform(x_test)
    return x_train,x_test

#创建逻辑回归模型并训练
def Logit_Model(x_train,y_train,x_test,y_test):
    model_line=LogisticRegression()
    model_line.fit(x_train,y_train.astype("int"))
    #模型保存
    joblib.dump(model_line,"./Model_Save/model_line.pkl")
    y_pred=model_line.predict(x_test)
    print(y_pred)
    print(model_line.score(x_test,y_test.astype("int")))
    print(accuracy_score(y_test,y_pred))
    #查准率
    precision = precision_score(y_test, y_pred, average='macro')
    #查全率（召回率）
    recall = recall_score(y_test, y_pred, average='micro')
    #F1得分
    f1 = f1_score(y_test, y_pred, average='weighted')
    print("查准率：",precision,"      召回率：",recall,"     F1得分：",f1)
    return 0

#创建决策树模型并进行训练
def Tree_Model(x_train,y_train,x_test,y_test):
    model_tree=DecisionTreeClassifier()
    model_tree.fit(x_train,y_train.astype("int"))
    #模型保存
    joblib.dump(model_tree,"./Model_Save/model_tree.pkl")
    pred_tree=model_tree.predict(x_test)
    score_tree=model_tree.score(x_test,y_test.astype("int"))
    print(score_tree)
    # 查准率
    precision = precision_score(y_test, pred_tree, average='macro')
    # 查全率（召回率）
    recall = recall_score(y_test, pred_tree, average='micro')
    # F1得分
    f1 = f1_score(y_test, pred_tree, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
    return 0

#创建朴素贝叶斯模型并进行训练
def NB_Model(x_train,y_train,x_test,y_test):
    model_bayes=GaussianNB()
    model_bayes.fit(x_train,y_train.astype("int"))
    pred_bayes=model_bayes.predict(x_test)
    score_bayes=model_bayes.score(x_test,y_test.astype("int"))
    print(score_bayes)
    #查准率
    precision = precision_score(y_test, pred_bayes, average='macro')
    #查全率（召回率）
    recall = recall_score(y_test, pred_bayes, average='micro')
    #F1得分
    f1 = f1_score(y_test, pred_bayes, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
    return 0


#创建K近邻模型并进行训练预测
def KNN_Model(x_train,y_train,x_test,y_test):
    model_Knn=KNeighborsClassifier()
    model_Knn.fit(x_train,y_train.astype("int"))
    pred_Knn=model_Knn.predict(x_test)
    score_Knn=model_Knn.score(x_test,y_test.astype("int"))
    print(score_Knn)
    #查准率
    precision = precision_score(y_test, pred_Knn, average='macro')
    #查全率（召回率）
    recall = recall_score(y_test, pred_Knn, average='micro')
    #F1得分
    f1 = f1_score(y_test, pred_Knn, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
    return 0


#创建svm分类器并进行训练预测
def SVM_Model(x_train,y_train,x_test,y_test):
    model_svm=svm.SVC()
    model_svm.fit(x_train,y_train.astype("int"))
    pred_svm=model_svm.predict(x_test)
    score_svm=model_svm.score(x_test,y_test.astype("int"))
    print(score_svm)
    #查准率
    precision = precision_score(y_test, pred_svm, average='macro')
    #查全率（召回率）
    recall = recall_score(y_test, pred_svm, average='micro')
    #F1得分
    f1 = f1_score(y_test, pred_svm, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
    return 0


#创建随机森林模型并进行训练
#分类器：随机森林
def RTree_Model(x_train,y_train,x_test,y_test):
    model_random=RandomForestClassifier(n_estimators=200)
    model_random.fit(x_train,y_train.astype("int"))
    pred_random=model_random.predict(x_test)
    score_random=model_random.score(x_test,y_test.astype("int"))
    print(score_random)
    #查准率
    precision = precision_score(y_test, pred_random, average='macro')
    #查全率（召回率）
    recall = recall_score(y_test, pred_random, average='micro')
    #F1得分
    f1 = f1_score(y_test, pred_random, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
    return 0


#创建GBDT分类器并训练预测
def GBDT_Model(x_train,y_train,x_test,y_test):
    model_GBDT=GradientBoostingClassifier(n_estimators=200)
    model_GBDT.fit(x_train,y_train.astype("int"))
    pred_GBDT=model_GBDT.predict(x_test)
    score_GBDT=model_GBDT.score(x_test,y_test.astype("int"))
    print(score_GBDT)
    #查准率
    precision = precision_score(y_test, pred_GBDT, average='macro')
    #查全率（召回率）
    recall = recall_score(y_test, pred_GBDT, average='micro')
    #F1得分
    f1 = f1_score(y_test, pred_GBDT, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
    return 0


#创建AdaBoost分类器并训练预测
def AdaBoost_Model(x_train,y_train,x_test,y_test):
    model_AB=AdaBoostClassifier()
    model_AB.fit(x_train,y_train.astype("int"))
    pred_AB=model_AB.predict(x_test)
    score_AB=model_AB.score(x_test,y_test.astype("int"))
    print(score_AB)
    #查准率
    precision = precision_score(y_test, pred_AB, average='macro')
    #查全率（召回率）
    recall = recall_score(y_test, pred_AB, average='micro')
    #F1得分
    f1 = f1_score(y_test, pred_AB, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
    return 0


#创建linear discriminat analysis模型并进行训练预测
def LDA_Model(x_train,y_train,x_test,y_test):
    model_lda=LinearDiscriminantAnalysis()
    model_lda.fit(x_train,y_train.astype("int"))
    pred_lda=model_lda.predict(x_test)
    score_lda=model_lda.score(x_test,y_test.astype("int"))
    print(score_lda)
    #查准率
    precision = precision_score(y_test, pred_lda, average='macro')
    #查全率（召回率）
    recall = recall_score(y_test, pred_lda, average='micro')
    #F1得分
    f1 = f1_score(y_test, pred_lda, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
    return 0


#创建QDA模型并进行训练预测
def QDA_Model(x_train,y_train,x_test,y_test):
    model_qda=QuadraticDiscriminantAnalysis()
    model_qda.fit(x_train,y_train.astype("int"))
    pred_qda=model_qda.predict(x_test)
    score_qda=model_qda.score(x_test,y_test.astype("int"))
    print(score_qda)
    #查准率
    precision = precision_score(y_test, pred_qda, average='macro')
    #查全率（召回率）
    recall = recall_score(y_test, pred_qda, average='micro')
    #F1得分
    f1 = f1_score(y_test, pred_qda, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
    return 0

#创建M贝叶斯模型并进行训练预测
def MNB_Model(x_train,y_train,x_test,y_test):
    model_mbayes=MultinomialNB(alpha=0.01)
    model_mbayes.fit(x_train,y_train.astype("int"))
    pred_mbayes=model_mbayes.predict(x_test)
    score_mbayes=model_mbayes.score(x_test,y_test.astype("int"))
    print(score_mbayes)
    #查准率
    precision = precision_score(y_test, pred_mbayes, average='macro')
    #查全率（召回率）
    recall = recall_score(y_test, pred_mbayes, average='micro')
    #F1得分
    f1 = f1_score(y_test, pred_mbayes, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
    return 0


if __name__ == '__main__':
    orgin_data = load_data("./train_data/orgin_data_first005.csv")
    orgin_data_first=Selected_fields(orgin_data)
    x_train, x_test, y_train, y_test=x_y_data(orgin_data_first)
    # x_train, x_test=stand_data(x_train,x_test)
    # Tree_Model(x_train,y_train,x_test,y_test)
    model_tree=joblib.load("./Model_Save/model_tree.pkl")
    pred_tree = model_tree.predict(x_test)
    score_tree = model_tree.score(x_test, y_test.astype("int"))
    print(score_tree)
    # 查准率
    precision = precision_score(y_test, pred_tree, average='macro')
    # 查全率（召回率）
    recall = recall_score(y_test, pred_tree, average='micro')
    # F1得分
    f1 = f1_score(y_test, pred_tree, average='weighted')
    print("查准率：", precision, "      召回率：", recall, "     F1得分：", f1)
