import pandas as pd


# 读取训练集和测试集数据
orgin_data = pd.read_csv("./train_data/orgin_data_first0001.csv")

print(orgin_data.columns)
count1 = 0
count0 = 0
for i in orgin_data["Label"]:
    if i == 1:
        count1 += 1
    elif i == 0:
        count0 += 1
print(count1, count0)


print(count0 // count1)


print(orgin_data.head())


print(orgin_data.iloc[0][1])


#重采样数据，将数据集中是僵尸网络的数据进行复制，从而尽可能保证正负样例比的1:1情况。
count = 0
print(len(orgin_data))
for i in range(len(orgin_data)):
    if orgin_data.iloc[i][8] == 1:
        count += 1
        print(count)
        #创建一个DataFrame用于保存中间数据
        insert_data=pd.DataFrame({
            'Day':orgin_data.iloc[i][0],
            'Hour':orgin_data.iloc[i][1],
            'Dur':orgin_data.iloc[i][2],
            'Proto':orgin_data.iloc[i][3],
            'Dir':orgin_data.iloc[i][4],
            'TotPkts':orgin_data.iloc[i][5],
            'TotBytes':orgin_data.iloc[i][6],
            'SrcBytes':orgin_data.iloc[i][7],
            'Label':orgin_data.iloc[i][8]
        } for j in range(66))
        print(insert_data)
        #将中间数据插入到DataFrame中并一定要注意使用一个新的变量（指针承接）
        orgin_data=orgin_data.append(insert_data,ignore_index=True)


print(len(orgin_data))

#保存数据集
orgin_data.to_csv("./train_data/train_data/orgin_data_first00001.csv")



