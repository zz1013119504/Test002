from __future__ import print_function
import numpy as np
import pandas as pd
from pyhive import hive
from impala.dbapi import connect
from pyspark import SparkContext
from pyspark import *
import os

if __name__ == '__main__':


    print(os.environ['SPARK_HOME'])
    print(os.environ['HADOOP_HOME'])

    sc = SparkContext("spark://node01:7077")
    rdd = sc.parallelize("hello Pyspark world".split(' '))
    counts = rdd.map(lambda word: (word, 1)) \
        .reduceByKey(lambda a, b: a + b)
    counts.saveAsTextFile('/usr/lib/spark/out')
    counts.foreach(print)

    sc.stop()
