# from scapy.all import *

# from tensorflow.keras import Sequential
# from tensorflow.keras.layers import Dense,LSTM,Dropout
# import pandas as pd
# from sklearn.model_selection import train_test_split
# from sklearn.metrics import mean_squared_error,accuracy_score
# import matplotlib.pyplot as plt
# pkts=rdpcap("./data/botnet-capture-20110810-neris.pcap")

# print([[1 for i in range(66)],[2 for i in range(66)]])
import pandas as pd
data=[[1,2,3],[3,4,5],[5,6,7],[7,8,9]]
data_pd=pd.DataFrame(data=data,columns=["one","two","three"])
print(data_pd)
insert_pd_data=pd.DataFrame(
    {
        "one":i,
        "two":i+1,
        "three":i+2
    } for i in range(9,12)
)
print(insert_pd_data)
data_pd=data_pd.append(insert_pd_data,ignore_index=True)
print(data_pd)